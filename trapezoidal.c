#include <stdio.h>
#include <math.h>


double function(float n) {
    return 1 / (1 + (n*n));
}


int main() {
    int n, count=0;
    float lower_bound, upper_bound, total_sum = 0, sum  = 0;
    printf("Enter number of intervals: ");
    scanf("%d", &n);

    printf("Enter lower limit and upper limit of the function: ");
    scanf("%f %f", &lower_bound, &upper_bound);

    total_sum += function(lower_bound) + function(upper_bound);
    printf("Count\tX\t\tY\n%d\t%f\t%f\n", count++, lower_bound, function(lower_bound));

    double diff = (upper_bound - lower_bound) / n;
    
    for (lower_bound += diff; lower_bound < upper_bound; lower_bound += diff) {
	printf("%d\t%f\t%f\n", count++, lower_bound, function(lower_bound));
	sum += function(lower_bound);
    }
    printf("%d\t%f\t%f\n", count, lower_bound, function(lower_bound));

    total_sum += 2*sum;

    printf("Root of the equation is %f\n", (total_sum * diff) / 2);
    return 0;
}
