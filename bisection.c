#include <stdio.h>
#include <math.h>


double function(double x) {
	return x*x*x - 4*x - 9;
}


double func(double x) {
	return x*x - 5;
}


int main() {
	int allowed_iterations, iterations = 1;
	float start_interval, end_interval, allowed_error, midpoint = 0, prev_midpoint;

	printf("Enter the lower-bound and upper-bound of the interval: ");
	scanf("%f %f", &start_interval, &end_interval);

	printf("Enter total number of allowed iterations: ");
	scanf("%d", &allowed_iterations);

	printf("Enter allowed error: ");
	scanf("%f", &allowed_error);

	printf("Iteration\tLower Bound\tUpper Bound\tMidpoint\n");

	for (; iterations <= allowed_iterations; ++iterations) {
		printf("%d\t\t%8.4f\t\t%8.4f\t\t%8.4f\n", iterations, start_interval, end_interval, midpoint);
		prev_midpoint = midpoint;
		midpoint = (start_interval + end_interval) / 2;

		if (func(start_interval) * func(midpoint) < 0)
			end_interval = midpoint;
		else
			start_interval = midpoint;

		if (fabs(midpoint - prev_midpoint) < allowed_error) {
			printf("Root is %8.4f\n", midpoint);
			return 0;
		}
	}

	printf("Insufficient Iterations\n");
	return 0;
}
