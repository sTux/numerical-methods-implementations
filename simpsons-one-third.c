#include <stdio.h>
#include <math.h>


double function(float n) {
	return 1 / (1 + n);
}


int main() {
    int n, count = 1;  // Count just keeps track of number of iterations
    float lower_bound, upper_bound, diff;
    printf("Enter n: ");
    scanf("%d", &n);

    printf("Enter lower limit and upper limit of the function: ");
    scanf("%f %f", &lower_bound, &upper_bound);

    diff = (upper_bound - lower_bound) / n;
    // Create the accumulator for summation of odd position values and even position values and initialize the total sum
    double total_sum = function(lower_bound) + function(upper_bound), odd_sum = 0, even_sum = 0;

    printf("Count\tX\t\tY\n");
    printf("%d\t%f\t%f\n", count-1, lower_bound, function(lower_bound));

    // With every iteration calculate the value of the function and add it the respective accumulator
    for (lower_bound += diff; lower_bound < upper_bound; lower_bound += diff) {
	printf("%d\t%f\t%f\n", count, lower_bound, function(lower_bound));
	if (count%2)
	    odd_sum += function(lower_bound);
	else
	    even_sum += function(lower_bound);
	count++;
    }
    printf("%d\t%f\t%f\n", count++, lower_bound, function(lower_bound));

    total_sum += 4*odd_sum + 2*even_sum;

    printf("Root of the equaltion %f\n", total_sum * (diff / 3.0));
    return 0;
}
