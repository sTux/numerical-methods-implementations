#include <stdio.h>
#include <math.h>


float function(float x) {
    return (x*x) + (2*x) - 2;
}


float derived(float x) {
    return (2*x) + 2;
}


int main() {
    float guess, error, prev_guess;
    int iteration = 1, max_iterations;

    printf("Enter initital guess of function x^2 + 2x - 2: ");
    scanf("%f", &guess);

    printf("Enter allowed error: ");
    scanf("%f", &error);

    printf("Maximum allowed iterations: ");
    scanf("%d", &max_iterations);

    printf("Iterations\tCurrent Guess\n");
    while (iteration <= max_iterations) {
        prev_guess = guess;
        guess = guess - (function(guess) / derived(guess));
        printf("%d\t\t%8.4f\n", iteration, guess);
        iteration++;

        if (fabs(guess - prev_guess) < error) {
            printf("Root is %8.4f\nApprox is %4.2f\n", guess, guess);
            return 0;
        }
    }

    printf("Equation didn't converged within %d iterations\n", max_iterations);
    return 0;
}
