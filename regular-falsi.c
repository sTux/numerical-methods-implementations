#include <stdio.h>
#include <math.h>


double func(double x) {
	return x*x*x - 25;
}


double function(double x) {
    return x*x - 5;
}


int main() {
    int iteration = 1;
    float lower_bound, upper_bound, midpoint = 0, prev_midpoint = 0, error;

    printf("Enter lower bound and upper bound of the interval: ");
    scanf("%f %f", &lower_bound, &upper_bound);

    printf("Enter allowed error: ");
    scanf("%f", &error);

    printf("Iterations\tLower Bound\tUpper Bound\tMidpoint\n");

    do {
        prev_midpoint = midpoint;
        midpoint = (lower_bound * function(upper_bound) - upper_bound * function(lower_bound)) / (function(upper_bound) - function(lower_bound));

        printf("%d\t\t%8.10f\t%8.10f\t%8.10f\n", iteration, lower_bound, upper_bound, midpoint);

        if (function(midpoint) * function(lower_bound) < 0)
            upper_bound = midpoint;
        else
            lower_bound = midpoint;
        iteration++;

    } while (fabs(midpoint - prev_midpoint) > error);

    printf("\nRoot is %8.10f\n", midpoint);
    return 0;
}
