#include <stdio.h>
#include <math.h>


double calculate_u(float u, int n) {
    double res = u;

    for (int i=1; i<n; ++i) {
        res *= (u - i);
    }

    return res;
}


int factorial(int n) {
    int res = 1;

    for (int i=2; i <= n; ++i)
        res *= i;

    return res;
}


int main() {
    int n;
    printf("Enter number of points: ");
    scanf("%d", &n);

    float x[n], y[n][n], value;

    // Get the values of all x
    for (int i=0; i < n; ++i) {
        printf("x[%d]: ", i+1);
        scanf("%f", &x[i]);
    }

    // Get the values of all f(x)
    for (int i=0; i < n; i++) {
        printf("y[%d]: ", i+1);
        scanf("%f", &y[i][0]);
    }

    // Calculate the difference table
    for (int col=1; col < n; ++col) {
        for (int row=0; row < n-1; ++row)
            y[row][col] = y[row+1][col-1] - y[row][col-1];
    }

    printf("Enter the value of interploated: ");
    scanf("%f", &value);

    float sum = y[0][0];
    float u = (value - x[0]) / (x[1] - x[0]);

    for (int i=1; i < n; ++i)
        sum += ((calculate_u(u, i) * y[0][i]) / factorial(i));

    printf("Interpolated sum is %f\n", sum);
    return 0;
}
